<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add staff::</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <h1>Add staff</h1>
        <p class="text-end"><a href="/all-staff" class="btn btn-primary">All staff</a></p>
            <div class="contaioner-fluid">
                <div class="card">
                    <div class="card-header">Add staff</div>
                
                    <div class="card-body">
                        <form action="{{ route('staff.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @if (Session::has('staff-added'))
                                <div class="alert alert-success">{{Session::get('staff-added')}}</div>
                            @endif
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name">
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="file">Choose profile image</label>
                                <input type="file" name="file">
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
</html>