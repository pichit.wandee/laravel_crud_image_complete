<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Staff;

class StaffController extends Controller
{
    //Store staff
    public function addStaff()
    {
        return view('add-staff');
    }
    public function storeStaff(Request $request)
    {
        $name = $request->name;
        $image = $request->file('file');
        $imageName = time().'.'.$image->extension();
        $image->move(public_path('image'),$imageName);

        $staff = new Staff();
        $staff->name = $name;
        $staff->profileimage = $imageName;
        $staff->save();

        return back()->with('staff-added','Staff has been saved.');
    }
    //Show staffs
    public function staffs()
    {
        $staffs = Staff::all();
        return view('all-staff',compact('staffs'));
    }
    //Edit staff
    public function editStaff($id)
    {
        $staff = Staff::find($id);
        return view('edit-staff',compact('staff'));
    }
    public function updateStaff(Request $request)
    {
        $name = $request->name;
        $image = $request->file('file');
        $imageName = time().'.'.$image->extension();
        $image->move(public_path('image'),$imageName);

        $staff = Staff::find($request->id);
        $staff->name = $name;
        $staff->profileimage = $imageName;
        $staff->save();

        return back()->with('staff-update','Staff record has been updated.');
    }
    //Delete staff
    public function deleteStaff($id)
    {
        $staff = Staff::find($id);
        unlink(public_path('image').'/'.$staff->profileimage);
        $staff->delete();

        return back()->with('staff.delete','This staff record has been deleted.');
    }
}
