<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaffController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Staff
Route::get('add-staff',[StaffController::class,'addStaff']);
Route::post('add-staff',[StaffController::class,'storeStaff'])->name('staff.store');
Route::get('all-staff',[StaffController::class,'staffs']);
Route::get('edit-staff/{id}',[StaffController::class,'editStaff']);
Route::post('update-staff',[StaffController::class,'updateStaff'])->name('staff.update');
Route::delete('delete-staff/{id}',[StaffController::class,'deleteStaff'])->name('staff.delete');



